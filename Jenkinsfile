node {
 	// Clean workspace before doing anything
    deleteDir()

    try {
		stage ('Info') {
			showEnvironmentVariables()
		}

        stage ('Clone') {
			checkout scm
        }

		stage ('preparations') {
            try {
                deploySettings = getDeploySettings()
                echo 'Deploy settings were set'
            } catch(err) {
                println(err.getMessage());
                throw err
            }
		}

        stage ('Build') {
        	sh "tar -cvzf numpy.tar.gz ."
        }

        stage ('Tests') {
	        parallel 'static': {
	            sh "echo 'shell scripts to run static tests...'"
	        },
	        'unit': {
	            sh "echo 'shell scripts to run unit tests...'"
				sh "echo `ls ./`"
				sh "echo `ls /`"
				sh "echo `pwd`"
				// sh "pytest numpy/tests"
	        },
	        'integration': {
	            sh "echo 'shell scripts to run integration tests...'"
	        }
        }

		if (deploySettings) {
            stage ('Deploy') {
                if (deploySettings.type && deploySettings.version) {
                    // Deploy specific version to a specifc server (IGR or PRD)
                    notifyDeployedVersion(deploySettings.version)
                } else {
                    // Deploy to develop branch into IGR server
                    sh "ssh ${deploySettings.ssh} 'mg2-deployer release'"
                }
            }
        }
    } catch (err) {
        currentBuild.result = 'FAILED'
        notifyFailed()
        throw err
    }
}

def showEnvironmentVariables() {
    sh 'env | sort > env.txt'
    sh 'cat env.txt'
}

def getBranchDetails() {
    def branchDetails = [:]
    branchData = BRANCH_NAME.split('/')
    if (branchData.size() == 2) {
        branchDetails['type'] = branchData[0]
        branchDetails['version'] = branchData[1]
        return branchDetails
    }
    return branchDetails
}

def getDeploySettings() {
    def deploySettings = [:]
    branchDetails = getBranchDetails()
    deploySettings['type'] = branchDetails.type
    deploySettings['version'] = branchDetails.version
    return deploySettings
}

def notifyDeployedVersion(String version) {
  emailext (
      subject: "Deployed: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
      body: "DEPLOYED VERSION '${version}': Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]': Check console output at '${env.BUILD_URL}' [${env.BUILD_NUMBER}]",
      to: "romain.mikl.mpsi@gmail.com"
    )
}

def notifyFailed() {
  emailext (
      subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
      body: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]': Check console output at '${env.BUILD_URL}' [${env.BUILD_NUMBER}]",
      to: "romain.mikl.mpsi@gmail.com"
    )
}